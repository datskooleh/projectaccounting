﻿using NoName.Accounting.Client.WPF.Common;
using NoName.Accounting.Client.WPF.ViewModels;
using NoName.Accounting.Client.WPF.Views;
using System;
using System.Globalization;
using System.Windows;

namespace NoName.Accounting.Client.WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        MainWindowViewModel vm;

        public App()
        {
            var viewService = new DialogService();

            vm = new MainWindowViewModel(viewService);
            vm.Exit += Application.Current.Shutdown;

            Exit += OnExit;
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            MainWindow = new MainWindow
            {
                DataContext = vm
            };

            MainWindow.Show();

            base.OnStartup(e);
        }

        private void OnExit(object sender, ExitEventArgs e)
        {
            vm.Exit -= Application.Current.Shutdown;

            Exit -= OnExit;
        }
    }
}
