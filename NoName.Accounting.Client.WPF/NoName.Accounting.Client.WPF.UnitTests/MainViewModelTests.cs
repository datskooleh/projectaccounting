﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NoName.Accounting.Client.WPF.Common;
using NoName.Accounting.Client.WPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoName.Accounting.Client.WPF.UnitTests
{
    [TestClass]
    public class MainViewModelTests
    {
        MainWindowViewModel mainViewModel = new MainWindowViewModel(new DialogService());

        [TestMethod]
        public void AddTab()
        {
            mainViewModel.AddNewTabCommand.Execute("MainWindow");
            mainViewModel.AddNewTabCommand.Execute("MainWindow");
            mainViewModel.AddNewTabCommand.Execute("ExternalServiceWindow");
            mainViewModel.AddNewTabCommand.Execute("SellWindow");
            mainViewModel.AddNewTabCommand.Execute("Base");

            Assert.AreEqual(mainViewModel.OpenedTabs.Count, 3);
        }

        [TestMethod]
        public void RemoveTab()
        {
            mainViewModel.AddNewTabCommand.Execute("MainWindow");
            mainViewModel.AddNewTabCommand.Execute("ExternalServiceWindow");
            mainViewModel.AddNewTabCommand.Execute("SellWindow");

            mainViewModel.CloseTabCommand.Execute(mainViewModel.OpenedTabs.Last());
            Assert.AreEqual(mainViewModel.OpenedTabs.Count, 2);
        }

        [TestMethod]
        public void SelectionChanges()
        {
            mainViewModel.AddNewTabCommand.Execute("MainWindow");
            Assert.AreEqual(mainViewModel.SelectedTab.Name, "MainWindow");

            mainViewModel.AddNewTabCommand.Execute("ExternalServiceWindow");
            Assert.AreEqual(mainViewModel.SelectedTab.Name, "ExternalServiceWindow");

            mainViewModel.CloseTabCommand.Execute(mainViewModel.OpenedTabs.Last());
            Assert.AreEqual(mainViewModel.SelectedTab.Name, "MainWindow");
        }
    }
}
