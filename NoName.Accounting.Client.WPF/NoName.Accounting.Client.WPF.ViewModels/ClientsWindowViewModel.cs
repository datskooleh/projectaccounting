﻿using NoName.Accounting.Client.WPF.Common;
using NoName.Accounting.Client.WPF.Common.Interfaces;
using NoName.Accounting.Client.WPF.Models;
using NoName.Accounting.Client.WPF.ViewModels.DialogsViewModels;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using ClientModel = NoName.Accounting.Client.WPF.Models.Client;

namespace NoName.Accounting.Client.WPF.ViewModels
{
    public sealed class ClientsWindowViewModel : BaseViewModel
    {
        #region Fields
        private ClientModel _selectedItem;
        private IDialogService _dialogService;
        #endregion //Fields

        #region Properties
        public ObservableCollection<ClientModel> Items { get; set; }

        public ICommand AddCommand { get; set; }

        public ICommand EditCommand { get; set; }

        public ICommand RemoveCommand { get; set; }

        public ClientModel SelectedItem
        {
            get { return _selectedItem; }
            set { SetValue(ref _selectedItem, value); }
        }
        #endregion //Properties

        #region Object creation and commands initialization
        public ClientsWindowViewModel(IDialogService dialogService)
        {
            Items = new ObservableCollection<ClientModel>();

            var cm = new ClientModel()
            {
                FirstName = "Oleh",
                LastName = "Datsko",
                Phone = "2165461",
                Address = new Address()
                {
                    City = "Byrshtyn",
                    Street = "Stusa",
                    House = "10",
                }
            };
            Items.Add(cm);

            InitializeCommand();

            _dialogService = dialogService;
        }

        private void InitializeCommand()
        {
            AddCommand = new RelayCommand(AddNewClient, () => true);
            EditCommand = new RelayCommand(EditClient, HasSelection);
            RemoveCommand = new RelayCommand(RemoveClient, HasSelection);
        }
        #endregion //Object creation and commands initialization

        #region Commands processing methods
        private bool HasSelection()
        {
            return SelectedItem != null;
        }

        private void RemoveClient()
        {
            _dialogService.ShowMessage("Do you realy want to remove " + SelectedItem.FirstName + ' ' + SelectedItem.LastName + "?",
                                       "Item remowing",
                                       callback: RemoveItemMessageProcessing,
                                       showYesNo: true);
        }

        private void EditClient()
        {
            ClientDialogViewModel viewModel = new ClientDialogViewModel(SelectedItem);
            var result = _dialogService.ShowDialogView("Editing " + SelectedItem.FirstName + ' ' + SelectedItem.LastName, viewModel);

            if (result)
                NoName.Accounting.Client.WPF.Common.Utilities.Copy.CopyObject(viewModel.Item, SelectedItem);
        }

        private void AddNewClient()
        {
            ClientDialogViewModel viewModel = new ClientDialogViewModel();

            var result = _dialogService.ShowDialogView("Creating item", viewModel);

            if (result)
            {
                var newRepair = viewModel.Item;
                Items.Add(newRepair);

                SelectedItem = newRepair;
            }
        }
        #endregion //Commands processing methods

        #region Private methods
        private void RemoveItemMessageProcessing(Boolean decision)
        {
            if (decision)
            {
                var index = Items.IndexOf(SelectedItem);

                Items.Remove(SelectedItem);

                if (index > 0)
                {
                    if (index >= Items.Count)
                        --index;
                    SelectedItem = Items[index];
                }
            }
        }
        #endregion //Private methods
    }
}
