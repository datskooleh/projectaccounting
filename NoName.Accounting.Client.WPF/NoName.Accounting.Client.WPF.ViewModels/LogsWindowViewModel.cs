﻿using NoName.Accounting.Client.WPF.Common;
using NoName.Accounting.Client.WPF.Models;
using System.Collections.ObjectModel;

namespace NoName.Accounting.Client.WPF.ViewModels
{
    public sealed class LogsWindowViewModel : BaseViewModel
    {
        #region Properties
        public ObservableCollection<ArticleLog> Items { get; set; }
        #endregion //Properties

        public LogsWindowViewModel()
        {
            Items = new ObservableCollection<ArticleLog>();
        }
    }
}
