﻿using NoName.Accounting.Client.WPF.Common;
using NoName.Accounting.Client.WPF.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Windows.Input;

namespace NoName.Accounting.Client.WPF.ViewModels
{
    public sealed class MainWindowViewModel : BaseViewModel
    {
        #region Fields
        private Dictionary<String, TypeInfo> _availableViewModels;
        private BaseViewModel _selectedTab;
        private IDialogService _viewService;
        #endregion //Fields

        #region Properties
        public ICommand AddNewTabCommand { get; set; }
        public ICommand CloseTabCommand { get; set; }
        public ICommand ExitApplicationCommand { get; set; }
        public ObservableCollection<BaseViewModel> OpenedTabs { get; private set; }
        public BaseViewModel SelectedTab
        {
            get { return _selectedTab; }
            set { SetValue(ref _selectedTab, value); }
        }
        #endregion //Properties

        #region Events
        public Action Exit;
        #endregion //Events

        #region Object creation and command initialization
        public MainWindowViewModel(IDialogService viewService)
        {
            _availableViewModels = LoadAvailableViewModels();

            OpenedTabs = new ObservableCollection<BaseViewModel>();
            
            InitializeCommands();

            _viewService = viewService;
        }

        private Dictionary<String, TypeInfo> LoadAvailableViewModels()
        {
            var assembly = Assembly.GetAssembly(this.GetType());
            var viewModels = assembly.DefinedTypes
                .Where(x => x.BaseType.Name.Equals("BaseViewModel"))
                .Select(x => x)
                .ToDictionary(x => x.Name.Replace("ViewModel", ""));

            return viewModels;
        }

        private void InitializeCommands()
        {
            AddNewTabCommand = new RelayCommand<String>(AddTab, () => true);
            CloseTabCommand = new RelayCommand<BaseViewModel>(CloseTab, () => true);
            ExitApplicationCommand = new RelayCommand(ExitApplication, () => true);
        }
        #endregion //Object creation and command initialization

        #region Commands processing method
        private void AddTab(String value)
        {
            if (_availableViewModels.ContainsKey(value))
            {
                var openedTab = OpenedTabs.SingleOrDefault(x => x.Name.Equals(value));

                if (openedTab == null)
                {
                    var hasConstructorWithoutParameters = _availableViewModels[value].GetConstructors().Any(x => x.GetParameters().Length == 0);

                    if (hasConstructorWithoutParameters)
                        openedTab = Activator.CreateInstance(_availableViewModels[value]) as BaseViewModel;
                    else
                        openedTab = Activator.CreateInstance(_availableViewModels[value], _viewService) as BaseViewModel;

                    OpenedTabs.Add(openedTab);
                    SelectedTab = openedTab;
                }
                else SelectedTab = openedTab;
            }
        }

        private void CloseTab(BaseViewModel value)
        {
            var tabToBeClosed = OpenedTabs.SingleOrDefault(x => x.Name.Equals(value.Name));

            if (tabToBeClosed != null)
            {
                if (SelectedTab.Name.Equals(tabToBeClosed.Name))
                    ChangeSelectedTab();

                OpenedTabs.Remove(tabToBeClosed);
            }
        }
        #endregion //Commands processing method

        #region Private methods
        private void ChangeSelectedTab()
        {
            var index = OpenedTabs.IndexOf(SelectedTab);

            if (index < OpenedTabs.Count - 1) SelectedTab = OpenedTabs[index + 1];
            else if (index > 0) SelectedTab = OpenedTabs[index - 1];
        }

        private void ExitApplication()
        {
            if (Exit != null) Exit();
        }
        #endregion //Private methods
    }
}
