﻿using NoName.Accounting.Client.WPF.Common;
using System;

namespace NoName.Accounting.Client.WPF.ViewModels
{
    public class BaseDialogViewModel : BaseViewModel
    {
        private Boolean? _dialogResult;
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            set { SetValue(ref _dialogResult, value); }
        }
    }
}
