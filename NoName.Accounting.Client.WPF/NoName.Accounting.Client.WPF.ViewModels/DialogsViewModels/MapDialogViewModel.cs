﻿using Microsoft.Maps.MapControl.WPF;
using NoName.Accounting.Client.WPF.Common;
using NoName.Accounting.Client.WPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace NoName.Accounting.Client.WPF.ViewModels.DialogsViewModels
{
    public class MapDialogViewModel : BaseDialogViewModel
    {
        #region Fields
        private Pushpin _startPushpin;
        private Pushpin _endPushpin;

        private String bingMapsKey = "ArY310hHetMJN-V5o-JpkgcFdgFiX-K_tnAisthJVhieCr4GJ_bXAYHwrgVmx8yR";
        private String _startLocationAddress;
        private String _endLocationAddress;
        private String _centeredLocation;
        private String _homeLocation;

        private Int32 _zoomLevel = 14;

        private Double _totalPrice;
        private Double _routeLength;
        private Double _pricePerKm;
        private Boolean _isTwoWay;

        private PathCost _item;
        #endregion //Fields

        #region Properties
        public String StartLocationAddress
        {
            get { return _startLocationAddress; }
            set
            {
                if (SetValue(ref _startLocationAddress, value))
                {
                    //Consider search on user input
                }
            }
        }

        public Pushpin StartPushpin
        {
            get { return _startPushpin; }
            set { SetValue(ref _startPushpin, value); }
        }

        public String EndLocationAddress
        {
            get { return _endLocationAddress; }
            set
            {
                if (SetValue(ref _endLocationAddress, value))
                {
                    //Consider search on user input
                }
            }
        }

        public Pushpin EndPushpin
        {
            get { return _endPushpin; }
            set { SetValue(ref _endPushpin, value); }
        }

        public Double RouteLength
        {
            get { return _routeLength; }
            set
            {
                if (SetValue(ref _routeLength, value))
                    TotalPrice = RouteLength / 100 * PricePerKm * (IsTwoWay ? 2 : 1);
            }
        }

        public Int32 ZoomLevel
        {
            get { return _zoomLevel; }
            set { SetValue(ref _zoomLevel, value); }
        }

        public String CenteredLocation
        {
            get
            {
                return String.IsNullOrWhiteSpace(_centeredLocation)
                    ? HomeLocation
                    : _centeredLocation;
            }
            set { SetValue(ref _centeredLocation, value); }
        }

        public String HomeLocation
        {
            get { return _homeLocation; }
            private set
            {
                if (SetValue(ref _homeLocation, value))
                    CenteredLocation = String.Copy(_homeLocation);
            }
        }

        public Boolean IsTwoWay
        {
            get { return _isTwoWay; }
            set
            {
                if (SetValue(ref _isTwoWay, value))
                    TotalPrice = PricePerKm * RouteLength * (IsTwoWay ? 2 : 1);
            }
        }

        public Double PricePerKm
        {
            get { return _pricePerKm; }
            set
            {
                if (SetValue(ref _pricePerKm, value))
                    TotalPrice = PricePerKm * RouteLength * (IsTwoWay ? 2 : 1);
            }
        }

        public Double TotalPrice
        {
            get { return _totalPrice; }
            set { SetValue(ref _totalPrice, value); }
        }

        public PathCost Item
        {
            get { return _item; }
            set { SetValue(ref _item, value); }
        }

        public ObservableCollection<Pushpin> Pushpins { get; set; }

        public ICommand StartLocationConfirmedCommand { get; private set; }

        public ICommand EndLocationConfirmedCommand { get; private set; }

        public ICommand SaveCommand { get; private set; }

        public ICommand CancelCommand { get; private set; }
        #endregion //Properties

        #region Object creation and command initialization
        public MapDialogViewModel()
        {
            _centeredLocation = String.Empty;
            _homeLocation = String.Empty;

            var geoWatcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High);
            geoWatcher.StatusChanged += GeoWatcherStatusChanged;
            geoWatcher.Start();

            Pushpins = new ObservableCollection<Pushpin>();

            StartPushpin = new Pushpin();
            StartPushpin.Location = new Microsoft.Maps.MapControl.WPF.Location();
            Pushpins.Add(StartPushpin);

            EndPushpin = new Pushpin();
            EndPushpin.Location = new Microsoft.Maps.MapControl.WPF.Location();
            Pushpins.Add(EndPushpin);

            Item = new PathCost();

            InitializeCommands();
        }

        public MapDialogViewModel(PathCost pathCost)
            : this()
        {
            Common.Utilities.Copy.CopyObject(pathCost, Item);
        }

        private void InitializeCommands()
        {
            StartLocationConfirmedCommand = new RelayCommand(SearchStartLocation, CanSearchStartLocation);
            EndLocationConfirmedCommand = new RelayCommand(SearchEndLocation, CanSearchEndLocation);
            SaveCommand = new RelayCommand(Save, CanSave);
            CancelCommand = new RelayCommand(Cancel, CanCancel);
        }

        private void Cancel()
        {
            DialogResult = false;
        }

        private Boolean CanCancel()
        {
            return true;
        }

        private void Save()
        {
            DialogResult = true;
        }

        private Boolean CanSave()
        {
            return !String.IsNullOrWhiteSpace(StartLocationAddress)
                && !String.IsNullOrWhiteSpace(EndLocationAddress)
                && PricePerKm > 0;
        }
        #endregion //Object creation and command initialization

        #region Event processing methods
        private void GeoWatcherStatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
        {
            if (e.Status == GeoPositionStatus.Ready)
            {
                using (var geoWatcher = sender as GeoCoordinateWatcher)
                {
                    HomeLocation = String.Format("{0}, {1}", geoWatcher.Position.Location.Latitude.ToString().Replace(',', '.'), geoWatcher.Position.Location.Longitude.ToString().Replace(',', '.'));

                    geoWatcher.StatusChanged -= GeoWatcherStatusChanged;
                    geoWatcher.Stop();

                    StartPushpin.Location.Latitude = geoWatcher.Position.Location.Latitude;
                    StartPushpin.Location.Longitude = geoWatcher.Position.Location.Longitude;

                    EndPushpin.Location.Latitude = geoWatcher.Position.Location.Latitude;
                    EndPushpin.Location.Longitude = geoWatcher.Position.Location.Longitude;
                }
            }
        }
        #endregion //Event processing methods

        #region Command methods
        private void SearchStartLocation()
        {
            Common.Utilities.Maps.GetLocationByAddress(StartLocationAddress, bingMapsKey, (coords) =>
            {
                if (coords != null)
                {
                    StartPushpin.Location.Latitude = coords.Item1;
                    StartPushpin.Location.Longitude = coords.Item2;

                    if (EndPushpin.Location.Latitude.Equals(Double.NaN))
                        CenteredLocation = coords.Item1.ToString().Replace(",", ".") + "," + coords.Item2.ToString().Replace(",", ".");
                    else
                        CenteredLocation = ((EndPushpin.Location.Latitude + coords.Item1) / 2).ToString().Replace(",", ".") + "," + ((EndPushpin.Location.Longitude + coords.Item2) / 2).ToString().Replace(",", ".");
                }
            });
        }

        private Boolean CanSearchStartLocation()
        {
            return !String.IsNullOrWhiteSpace(StartLocationAddress);
        }

        private void SearchEndLocation()
        {
            Common.Utilities.Maps.GetLocationByAddress(EndLocationAddress, bingMapsKey, (coords) =>
            {
                if (coords != null)
                {
                    EndPushpin.Location.Latitude = coords.Item1;
                    EndPushpin.Location.Longitude = coords.Item2;

                    CenteredLocation = ((StartPushpin.Location.Latitude + coords.Item1) / 2).ToString().Replace(",", ".") + "," + ((StartPushpin.Location.Longitude + coords.Item2) / 2).ToString().Replace(",", ".");
                }
            });
        }

        private Boolean CanSearchEndLocation()
        {
            return !String.IsNullOrWhiteSpace(EndLocationAddress);
        }
        #endregion //Command methods

        #region Test here
        private void Route()
        {
            NoName.Accounting.Client.WPF.Common.Utilities.Maps.GetRoute("40.783669,-73.964918", "40.72753,-73.99246", bingMapsKey, (obj) =>
            {
                //Common.Utilities.Maps.Route route = obj;

                if (obj != null)
                {
                    RouteLength = obj.travelDistance;

                    if (obj.itineraryItems != null && obj.itineraryItems.Count > 0)
                    {
                        var line = new MapPolyline();
                        line.Locations = new LocationCollection();
                        //line.Color = Colors.Red;

                        foreach (var location in obj.itineraryItems)
                            line.Locations.Add(new Microsoft.Maps.MapControl.WPF.Location(location.maneuverPoint.coordinates[0], location.maneuverPoint.coordinates[1]));
                    }
                }
            });
        }
        #endregion
    }
}
