﻿using NoName.Accounting.Client.WPF.Common;
using NoName.Accounting.Client.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace NoName.Accounting.Client.WPF.ViewModels.DialogsViewModels
{
    public sealed class ArticleItemDialogViewModel : BaseDialogViewModel
    {
        #region Fields
        private String _price;
        private String _count;
        #endregion //Fields

        #region Properties
        public Article Item { get; set; }
        
        public ICommand SaveCommand { get; set; }
        
        public ICommand CancelCommand { get; set; }

        public String Price
        {
            get { return _price; }
            set
            {
                Double res;
                if (Double.TryParse(value, out res))
                    Item.Price = res;
                SetValue(ref _price, value);
            }
        }

        public String Count
        {
            get { return _count; }
            set
            {
                Double res;
                if (Double.TryParse(value, out res))
                    Item.Count = res;
                SetValue(ref _count, value);
            }
        }
        #endregion //Properties

        #region Object creation and commands initialization
        public ArticleItemDialogViewModel()
        {
            Item = new Article();

            InitializeCommands();
        }

        public ArticleItemDialogViewModel(Article article)
            : this()
        {
            NoName.Accounting.Client.WPF.Common.Utilities.Copy.CopyObject(article, Item);
            Price = article.Price.ToString();
            Count = article.Count.ToString();
        }

        private void InitializeCommands()
        {
            SaveCommand = new RelayCommand(Save, CanSave);
            CancelCommand = new RelayCommand(Cancel, () => true);
        }
        #endregion //Object creation and commands initialization

        #region Commands processing methods
        private void Cancel()
        {
            DialogResult = false;
        }

        private bool CanSave()
        {
            Double parsed;
            return !String.IsNullOrWhiteSpace(Item.Title)
                   && Double.TryParse(Count, out parsed)
                   && parsed >= 0
                   && Double.TryParse(Price, out parsed)
                   && parsed >= 0;
        }

        private void Save()
        {
            DialogResult = true;
        }
        #endregion //Commands processing methods
    }
}
