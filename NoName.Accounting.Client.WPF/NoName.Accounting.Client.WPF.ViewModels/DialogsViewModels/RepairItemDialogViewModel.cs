﻿using NoName.Accounting.Client.WPF.Common;
using NoName.Accounting.Client.WPF.Models;
using NoName.Accounting.Client.WPF.Models.Enums;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace NoName.Accounting.Client.WPF.ViewModels.DialogsViewModels
{
    public class RepairItemDialogViewModel : BaseDialogViewModel
    {
        #region Private fields
        private RepairStatus _selectedStatus;
        private DateTime _originalDateAccepted;
        #endregion //Private fields

        #region Properties
        public List<String> AvailableStatuses { get; set; }
        public Repair Item { get; set; }
        public ICommand SaveCommand { get; set; }
        public ICommand CancelCommand { get; set; }
        public String SelectedStatus
        {
            get { return Item.Status.ToString(); }
            set
            {
                _selectedStatus = (RepairStatus)Enum.Parse(typeof(RepairStatus), value);
                Item.Status = _selectedStatus;
            }
        }

        private String _price;
        public String Price
        {
            get { return _price; }
            set
            {
                Double res;
                if (Double.TryParse(value, out res))
                    Item.Price = res;
                SetValue(ref _price, value);
            }
        }
        #endregion //Properties

        #region Object creation and initialization
        public RepairItemDialogViewModel()
        {
            InitializeCommands();

            Item = new Repair()
            {
                Date = DateTime.UtcNow
            };
            Price = "0";

            AvailableStatuses = new List<String>();
            AvailableStatuses.AddRange(Enum.GetNames(typeof(RepairStatus)));
        }

        public RepairItemDialogViewModel(Repair repair)
            : this()
        {
            NoName.Accounting.Client.WPF.Common.Utilities.Copy.CopyObject(repair, Item);
            Price = repair.Price.ToString();

            _originalDateAccepted = repair.Date;
        }

        private void InitializeCommands()
        {
            SaveCommand = new RelayCommand(Save, CanSave);
            CancelCommand = new RelayCommand(Cancel, () => true);
        }
        #endregion

        #region Command processing methods
        private void Cancel()
        {
            DialogResult = false;
        }

        private Boolean CanSave()
        {
            Double parsed;
            return !String.IsNullOrWhiteSpace(Item.Name)
                && Double.TryParse(Price, out parsed)
                && Item.Price > -1
                && Item.DateReturned.HasValue
                    ? Item.Date < Item.DateReturned.Value
                    : true;
        }

        private void Save()
        {
            DialogResult = true;
        }
        #endregion
    }
}
