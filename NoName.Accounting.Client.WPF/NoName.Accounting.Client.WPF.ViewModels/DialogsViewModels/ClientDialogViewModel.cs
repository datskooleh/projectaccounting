﻿using NoName.Accounting.Client.WPF.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace NoName.Accounting.Client.WPF.ViewModels.DialogsViewModels
{
    public sealed class ClientDialogViewModel : BaseDialogViewModel
    {
        #region Properties
        public Models.Client Item { get; set; }
        public ICommand SaveCommand { get; set; }

        public ICommand CancelCommand { get; set; }
        #endregion //Properties

        #region Object creation and commands initialization
        public ClientDialogViewModel()
        {
            Item = new Models.Client();

            InitializeCommands();
        }

        public ClientDialogViewModel(Models.Client item)
            : this()
        {
            NoName.Accounting.Client.WPF.Common.Utilities.Copy.CopyObject(item, Item);
        }

        private void InitializeCommands()
        {
            SaveCommand = new RelayCommand(Save, CanSave);
            CancelCommand = new RelayCommand(Cancel, () => true);
        }

        private bool CanSave()
        {
            var res = !String.IsNullOrWhiteSpace(Item.FirstName)
                && !String.IsNullOrWhiteSpace(Item.LastName)
                && !String.IsNullOrWhiteSpace(Item.Phone)
                && !String.IsNullOrWhiteSpace(Item.Address.City)
                && !String.IsNullOrWhiteSpace(Item.Address.Street)
                && !String.IsNullOrWhiteSpace(Item.Address.House)
                && (Item.Address.Appartment.HasValue
                ? Item.Address.Appartment.Value > 0
                : true);

            return res;
        }

        private void Cancel()
        {
            DialogResult = false;
        }

        private void Save()
        {
            DialogResult = true;
        }
        #endregion //Object creation and commands initialization
    }
}
