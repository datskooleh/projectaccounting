﻿using NoName.Accounting.Client.WPF.Common.Base;
using System;

namespace NoName.Accounting.Client.WPF.ViewModels
{
    public class BaseViewModel : BaseObject
    {
        public String Name { get { return GetType().Name.Replace("ViewModel", ""); } }

        public String TabName { get { return Name.Replace("Window", ""); } }
    }
}
