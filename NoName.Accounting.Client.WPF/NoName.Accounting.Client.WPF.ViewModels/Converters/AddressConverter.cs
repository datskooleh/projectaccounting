﻿using System;
using System.Windows.Data;

namespace NoName.Accounting.Client.WPF.ViewModels.Converters
{
    public sealed class AddressConverter : IMultiValueConverter
    {
        public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var address = String.Empty;

            for (var i = 0; i < values.Length - 1; i++)
            {
                if (values[i] != null)
                {
                    if (address.Length == 0) address += values[i];
                    else address += ", " + values[i];
                }
            }

            if (values[values.Length - 1] != null)
                address += "/" + values[values.Length - 1];

            return address;
        }

        public object[] ConvertBack(object value, System.Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
    }
}
