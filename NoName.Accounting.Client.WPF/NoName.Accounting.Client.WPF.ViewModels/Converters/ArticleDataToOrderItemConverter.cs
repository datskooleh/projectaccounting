﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace NoName.Accounting.Client.WPF.ViewModels.Converters
{
    public sealed class ArticleDataToOrderItemConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Console.WriteLine("ArticleDataToOrderItemConverter fired");
            foreach (var value in values)
            {
                Debug.WriteLine(value);
            }

            return values;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
