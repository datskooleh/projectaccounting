﻿using NoName.Accounting.Client.WPF.Common;
using NoName.Accounting.Client.WPF.Common.Interfaces;
using NoName.Accounting.Client.WPF.Models;
using NoName.Accounting.Client.WPF.Models.Enums;
using NoName.Accounting.Client.WPF.ViewModels.DialogsViewModels;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace NoName.Accounting.Client.WPF.ViewModels
{
    public class RepairWindowViewModel : BaseViewModel
    {
        #region Fields
        private IDialogService _dialogService;
        private RepairStatus _selectedStatus;
        private Repair _selectedItem;
        #endregion //Fields

        #region Properties
        public ObservableCollection<Repair> Items { get; private set; }

        public ICommand AddNewItemCommand { get; set; }

        public ICommand EditItemCommand { get; set; }

        public ICommand RemoveItemCommand { get; set; }

        public RepairStatus SelectedStatus
        {
            get { return _selectedStatus; }
            set { SetValue(ref _selectedStatus, value); }
        }

        public Repair SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                //if (value != null)
                //    Price = value.Price.ToString();
                //else
                //    Price = String.Empty;

                SetValue(ref _selectedItem, value);
            }
        }

        //public String Price
        //{
        //    get { return _price; }
        //    set { SetValue(ref _price, value); }
        //}
        #endregion //Properties

        #region Object creation and commands initialization
        public RepairWindowViewModel(IDialogService dialogService)
        {
            _dialogService = dialogService;

            InitializeCommands();

            Items = new ObservableCollection<Repair>();
            Items.Add(new Repair()
            {
                Name = "Testing item",
                Status = RepairStatus.Accepted,
                Description = "adfdfg dfsdf fdgdf gdfgfsds dfgdfg sfdfsf",
                Price = 254.84,
                Date = DateTime.Now
            });
        }

        private void InitializeCommands()
        {
            AddNewItemCommand = new RelayCommand(AddNewItem, CanAddItem);
            EditItemCommand = new RelayCommand(EditItem, CanChangeSelectedItem);
            RemoveItemCommand = new RelayCommand(RemoveItem, CanChangeSelectedItem);
        }
        #endregion //Object creation and commands initialization

        #region Commands processing methods
        private Boolean CanAddItem()
        {
            return true;
        }

        private void AddNewItem()
        {
            RepairItemDialogViewModel viewModel = new RepairItemDialogViewModel();

            var result = _dialogService.ShowDialogView("Creating item", viewModel);

            if (result)
            {
                var newRepair = viewModel.Item;
                Items.Add(newRepair);

                SelectedItem = newRepair;
            }
        }

        private bool CanChangeSelectedItem()
        {
            return SelectedItem != null;
        }

        private void RemoveItem()
        {
            _dialogService.ShowMessage("Do you realy want to remove " + SelectedItem.Name + "?",
                                       "Item remowing",
                                       callback: RemoveItemMessageProcessing,
                                       showYesNo: true);
        }

        private void EditItem()
        {
            RepairItemDialogViewModel viewModel = new RepairItemDialogViewModel(SelectedItem);
            var result = _dialogService.ShowDialogView("Editing " + SelectedItem.Name, viewModel);

            if (result)
            {
                NoName.Accounting.Client.WPF.Common.Utilities.Copy.CopyObject(viewModel.Item, SelectedItem);
            }
        }
        #endregion //Commands processing methods

        private void RemoveItemMessageProcessing(Boolean decision)
        {
            if (decision)
            {
                var index = Items.IndexOf(SelectedItem);

                Items.Remove(SelectedItem);

                if (index > 0)
                {
                    if (index >= Items.Count)
                        --index;
                    SelectedItem = Items[index];
                }
            }
        }
    }
}
