﻿using NoName.Accounting.Client.WPF.Common;
using NoName.Accounting.Client.WPF.Common.Interfaces;
using NoName.Accounting.Client.WPF.Models;
using NoName.Accounting.Client.WPF.ViewModels.DialogsViewModels;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace NoName.Accounting.Client.WPF.ViewModels
{
    public class ArticlesWindowViewModel : BaseViewModel
    {
        #region Fields
        private IDialogService _dialogService;
        private Article _selectedItem;
        #endregion //Fields

        #region Properties
        public ObservableCollection<Article> Items { get; private set; }

        public Article SelectedItem
        {
            get { return _selectedItem; }
            set { SetValue(ref _selectedItem, value); }
        }

        public ICommand AddCommand { get; set; }
        public ICommand EditCommand { get; set; }
        public ICommand RemoveCommand { get; set; }
        public ICommand ShowLogsCommand { get; set; }
        #endregion //Properties

        #region Object creation and commands initialization
        public ArticlesWindowViewModel(IDialogService dialogService)
        {
            _dialogService = dialogService;

            Items = new ObservableCollection<Article>();

            InitializeCommads();
        }

        private void InitializeCommads()
        {
            AddCommand = new RelayCommand(AddItem, () => true);
            EditCommand = new RelayCommand(EditItem, IsItemSelected);
            RemoveCommand = new RelayCommand(RemoveItem, IsItemSelected);
            ShowLogsCommand = new RelayCommand(ShowLogs, IsItemSelected);
        }
        #endregion //Object creation and commands initialization

        #region Commands processing methods
        private void AddItem()
        {
            var viewModel = new ArticleItemDialogViewModel();
            var result = _dialogService.ShowDialogView("Add article", viewModel);

            if (result)
            {
                var newItem = new Article();

                NoName.Accounting.Client.WPF.Common.Utilities.Copy.CopyObject(viewModel.Item, newItem);
                Items.Add(newItem);

                SelectedItem = newItem;
            }
        }

        private void EditItem()
        {
            var viewModel = new ArticleItemDialogViewModel(SelectedItem);
            var result = _dialogService.ShowDialogView("Editing " + SelectedItem.Title, viewModel);

            if (result) NoName.Accounting.Client.WPF.Common.Utilities.Copy.CopyObject(viewModel.Item, SelectedItem);
        }

        private void RemoveItem()
        {
            _dialogService.ShowMessage("Do you realy want to remove " + SelectedItem.Title + "?",
                                       "Item remowing",
                                       callback: RemoveItemMessageProcessing,
                                       showYesNo: true);
        }

        private void ShowLogs()
        {
            throw new System.NotImplementedException();
        }

        private Boolean IsItemSelected()
        {
            return SelectedItem != null;
        }
        #endregion //Commands processing methods

        #region Private methods
        private void RemoveItemMessageProcessing(Boolean decision)
        {
            if (decision)
            {
                var index = Items.IndexOf(SelectedItem);

                Items.Remove(SelectedItem);

                if (index > 0)
                {
                    if (index >= Items.Count)
                        --index;
                    SelectedItem = Items[index];
                }
            }
        }
        #endregion //Private methods
    }
}