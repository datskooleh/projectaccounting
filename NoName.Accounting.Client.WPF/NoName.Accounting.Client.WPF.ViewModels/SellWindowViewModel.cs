﻿using NoName.Accounting.Client.WPF.Common;
using NoName.Accounting.Client.WPF.Models;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace NoName.Accounting.Client.WPF.ViewModels
{
    public class SellWindowViewModel : BaseViewModel
    {
        #region Fields
        private OrderItem _selectedItem;
        private Article _availableArticleSelectedItem;
        private String _count;
        private String _price;
        private Double _totalPrice;
        #endregion //Fields

        #region Properties
        public ObservableCollection<Article> AvailableArticlesItems { get; set; }
        public Article AvailableArticleSelectedItem
        {
            get { return _availableArticleSelectedItem; }
            set
            {
                if (SetValue(ref _availableArticleSelectedItem, value))
                {
                    Price = String.Empty;
                    Count = String.Empty;
                }
            }
        }

        public ObservableCollection<OrderItem> Items { get; set; }
        public OrderItem SelectedItem
        {
            get { return _selectedItem; }
            set { SetValue(ref _selectedItem, value); }
        }

        public String Count
        {
            get { return _count; }
            set { SetValue(ref _count, value); }
        }
        public String Price
        {
            get { return _price; }
            set { SetValue(ref _price, value); }
        }

        public Double TotalPrice
        {
            get { return _totalPrice; }
            set { SetValue(ref _totalPrice, value); }
        }

        public ICommand AddItemToSellCommand { get; set; }
        public ICommand ConfirmSellCommand { get; set; }
        public ICommand RemoveFromSellCommand { get; set; }
        #endregion //Properties

        #region Object creation and commands initialization
        public SellWindowViewModel()
        {
            AvailableArticlesItems = new ObservableCollection<Article>()
            {
                new Article{
                  Count = 10,
                  Description = "Item 1",
                  Title = "Super item 1",
                  Price = 15.46,
                },
                new Article{
                  Count = 6,
                  Description = "Item 2",
                  Title = "Not so good item",
                  Price = 1.5,
                },
                new Article{
                  Count = 65,
                  Description = "Item 3",
                  Title = "Item to be good",
                  Price = 165.48,
                },
            };

            Items = new ObservableCollection<OrderItem>();

            InitializeCommands();
        }

        private void InitializeCommands()
        {
            AddItemToSellCommand = new RelayCommand(AddItem, CanAdd);
            ConfirmSellCommand = new RelayCommand(ConfirmSell, CanConfirmSell);
            RemoveFromSellCommand = new RelayCommand(RemoveFromSell, CanRemoveFromSell);
        }
        #endregion //Object creation and commands initialization

        #region Commands processing methods
        private bool CanRemoveFromSell()
        {
            return SelectedItem != null;
        }

        private void RemoveFromSell()
        {
            var articleItem = SelectedItem.Article;

            articleItem.Count += SelectedItem.Count;
            TotalPrice -= SelectedItem.TotalPrice;

            lock (Items)
                Items.Remove(SelectedItem);
        }

        private Boolean CanConfirmSell()
        {
            return Items.Count > 0;
        }

        private void ConfirmSell()
        {
            var order = new Order()
            {
                Date = DateTime.UtcNow,
                OrderItems = Items,
                Price = TotalPrice,
            };
        }

        private Boolean CanAdd()
        {
            Double res;

            var canProceed = AvailableArticleSelectedItem != null;

            var itemToSellCount = 0.0;

            if (canProceed)
            {
                lock (Items)
                {
                    OrderItem itemToSell = Items.SingleOrDefault(x => x.Article == AvailableArticleSelectedItem);
                    itemToSellCount = itemToSell == null ? 0 : itemToSell.Count;
                }
            }

            return canProceed
                && Double.TryParse(Count, out res)
                && res > 0
                && res <= (AvailableArticleSelectedItem.Count + itemToSellCount)
                && Double.TryParse(Price, out res)
                && res > 0;
        }

        private void AddItem()
        {
            var obj = Items.SingleOrDefault(x => x.Article == AvailableArticleSelectedItem);
            var convertedCount = Convert.ToDouble(this.Count);

            if (obj == null)
            {
                obj = new OrderItem()
                {
                    Article = AvailableArticleSelectedItem,
                    Count = convertedCount,
                    TotalPrice = convertedCount * AvailableArticleSelectedItem.Price,
                    Price = Convert.ToDouble(this.Price),
                };

                lock (Items)
                    Items.Add(obj);

                AvailableArticleSelectedItem.Count -= obj.Count;
            }
            else
            {
                TotalPrice -= obj.TotalPrice;
                AvailableArticleSelectedItem.Count += Math.Abs(obj.Count - convertedCount);

                obj.Count = convertedCount;
                obj.TotalPrice = convertedCount * AvailableArticleSelectedItem.Price;
                obj.Price = Convert.ToDouble(this.Price);
            }

            TotalPrice += obj.TotalPrice;
        }
        #endregion //Commands processing methods
    }
}
