﻿using Microsoft.Maps.MapControl.WPF;
using NoName.Accounting.Client.WPF.Common;
using NoName.Accounting.Client.WPF.Common.Interfaces;
using NoName.Accounting.Client.WPF.Models;
using NoName.Accounting.Client.WPF.ViewModels.DialogsViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Device.Location;
using System.Windows.Input;
using System.Windows.Media;

namespace NoName.Accounting.Client.WPF.ViewModels
{
    public class ExternalServiceWindowViewModel : BaseViewModel
    {
        #region Fields
        private IDialogService _dialogService;

        private Double _totalCost;
        private Double _routeLength;

        private PathCost _pathCost;
        private Consumable _selectedItem;
        #endregion //Fields

        #region Properties
        public Double TotalCost
        {
            get { return _totalCost; }
            set { SetValue(ref _totalCost, value); }
        }

        public Double RouteLength
        {
            get { return _routeLength; }
            set { SetValue(ref _routeLength, value); }
        }

        public Consumable SelectedItem
        {
            get { return _selectedItem; }
            set { SetValue(ref _selectedItem, value); }
        }

        public PathCost PathCost
        {
            get { return _pathCost; }
            set { SetValue(ref _pathCost, value); }
        }

        public ObservableCollection<Consumable> Items { get; private set; }

        public ICommand OpenMapDialogWindowCommand { get; private set; }

        public ICommand ClearRouteCostCommand { get; private set; }

        public ICommand RemoveFromConsumablesCommand { get; private set; }

        public ICommand ConfirmServiceCommand { get; private set; }
        //public ICommand StartLocationConfirmedCommand { get; set; }

        //public ICommand EndLocationConfirmedCommand { get; set; }
        #endregion //Properties

        #region Object creation and command initialization
        public ExternalServiceWindowViewModel(IDialogService dialogService)
        {
            _dialogService = dialogService;

            Items = new ObservableCollection<Consumable>();
            PathCost = new PathCost();

            InitializeCommands();
        }

        private void InitializeCommands()
        {
            OpenMapDialogWindowCommand = new RelayCommand(OpenMapDialogWindow, () => true);
            RemoveFromConsumablesCommand = new RelayCommand(RemoveConsumable, CanRemoveConsumable);
            ConfirmServiceCommand = new RelayCommand(ConfirmService, CanConfirmService);
            ClearRouteCostCommand = new RelayCommand(ClearRouteCost, CanClearRouteCost);
        }

        #endregion //Object creation and command initialization

        #region Command methods
        private void OpenMapDialogWindow()
        {
            MapDialogViewModel viewModel = new MapDialogViewModel();

            var result = _dialogService.ShowDialogView("Addng route", viewModel);

            if (result)
            {
                TotalCost -= PathCost.TotalCost;

                Common.Utilities.Copy.CopyObject(viewModel.Item, PathCost);

                TotalCost += PathCost.TotalCost;
            }
        }

        private void RemoveConsumable()
        {
            if (SelectedItem != null)
            {
                var index = Items.IndexOf(SelectedItem);

                Items.Remove(SelectedItem);

                if (index < Items.Count)
                    SelectedItem = Items[index];
                else if (index > 0)
                    SelectedItem = Items[index - 1];
                else
                    SelectedItem = null;
            }
        }

        private Boolean CanRemoveConsumable()
        {
            return SelectedItem != null;
        }

        private void ConfirmService()
        {
            throw new NotImplementedException();
        }

        private Boolean CanConfirmService()
        {
            return Items.Count > 0 || !String.IsNullOrWhiteSpace(PathCost.StartLocation.AddressLine);
        }

        private void ClearRouteCost()
        {
            TotalCost -= PathCost.TotalCost;

            PathCost = new PathCost();
        }

        private bool CanClearRouteCost()
        {
            return !String.IsNullOrWhiteSpace(PathCost.StartLocation.AddressLine);
        }
        #endregion //Command methods
    }
}
