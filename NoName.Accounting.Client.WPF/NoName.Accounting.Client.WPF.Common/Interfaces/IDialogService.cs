﻿using NoName.Accounting.Client.WPF.Common.Enums;
using System;
using System.Windows;

namespace NoName.Accounting.Client.WPF.Common.Interfaces
{
    public interface IDialogService
    {
        Boolean ShowDialogView<T>(String title, T viewModel) where T : class;

        void ShowMessage(String text,
                         String caption = "",
                         WindowMessageType messageType = WindowMessageType.Information,
                         Action<Boolean> callback = null,
                         Boolean showYesNo = false);
    }
}
