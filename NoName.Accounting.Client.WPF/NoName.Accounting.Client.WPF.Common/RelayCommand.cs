﻿using System;
using System.Windows.Input;

namespace NoName.Accounting.Client.WPF.Common
{
    public class RelayCommand<T> : ICommand where T: class
    {
        Func<Boolean> _canExecute;
        Action<T> _execute;

        public RelayCommand(Action<T> execute, Func<Boolean> canExecute)
        {
            _canExecute = canExecute;
            _execute = execute;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute != null
                ? _canExecute()
                : true;
        }

        public void Execute(object parameter)
        {
            if (_execute == null)
                throw new NullReferenceException("Execute method does not set");
            else
                _execute(parameter as T);
        }
    }

    public class RelayCommand : ICommand
    {
        Func<Boolean> _canExecute;
        Action _execute;

        public RelayCommand(Action execute, Func<Boolean> canExecute)
        {
            _canExecute = canExecute;
            _execute = execute;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute != null
                ? _canExecute()
                : true;
        }

        public void Execute(object parameter)
        {
            if (_execute == null)
                throw new NullReferenceException("Execute method does not set");
            else
                _execute();
        }
    }
}
