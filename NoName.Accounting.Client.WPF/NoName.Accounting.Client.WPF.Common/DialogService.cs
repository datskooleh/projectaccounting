﻿using NoName.Accounting.Client.WPF.Common.Enums;
using NoName.Accounting.Client.WPF.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Threading;

namespace NoName.Accounting.Client.WPF.Common
{
    public class DialogService : IDialogService
    {
        private readonly Dispatcher _dispatcher = Application.Current.Dispatcher;
        private Dictionary<String, TypeInfo> _availableViews = new Dictionary<String, TypeInfo>();

        public DialogService()
        {
            _availableViews = LoadAvailableViews();
        }

        private Dictionary<String, TypeInfo> LoadAvailableViews()
        {
            var assembly = Assembly.Load("NoName.Accounting.Client.WPF.Views");
            return assembly.DefinedTypes
                .Where(x => x.Name.Contains("Dialog"))
                .Select(x => x)
                .ToDictionary(x => x.Name);
        }

        public Boolean ShowDialogView<T>(String title, T viewModel) where T : class
        {
            ValidateViewModel<T>(viewModel);

            if (!_dispatcher.CheckAccess())
            {
                Boolean res = false;
                _dispatcher.Invoke(new Action(() => { res = ShowDialogView(title, viewModel); }));
                return res;
            }
            else
            {
                TypeInfo viewType;
                Window view;
                if (_availableViews.TryGetValue(viewModel.GetType().Name.Replace("ViewModel", ""), out viewType))
                {
                    view = Activator.CreateInstance(viewType) as Window;
                    if (view == null)
                        throw new InvalidCastException("Given windows is not a window type");

                    view.Title = title;
                    view.DataContext = viewModel;
                }
                else throw new InvalidOperationException("Can't open view for selected ViewModel");
                var result = view.ShowDialog();

                return result.HasValue && result.Value;
            }
        }

        public void ShowMessage(String text,
                         String caption = "",
                         WindowMessageType messageType = WindowMessageType.Information,
                         Action<Boolean> callback = null,
                         Boolean showYesNo = false)
        {
            var result = MessageBox
                .Show(text, caption, showYesNo ? MessageBoxButton.YesNo : MessageBoxButton.OK, ParseMessgeType(messageType));

            if (callback != null) callback(result == MessageBoxResult.Yes);
        }


        private static void ValidateViewModel<T>(T viewModel) where T : class
        {
            if (viewModel == null)
                throw new ArgumentNullException("Value can't be null");

            var viewModelType = viewModel.GetType();

            if (!viewModelType.BaseType.Name.Equals("BaseDialogViewModel"))
                throw new ArgumentException("Given object is not a view model object");
        }

        private MessageBoxImage ParseMessgeType(WindowMessageType messageType)
        {
            MessageBoxImage result;

            switch (messageType)
            {
                case WindowMessageType.Information:
                    result = MessageBoxImage.Information;
                    break;
                case WindowMessageType.Question:
                    result = MessageBoxImage.Question;
                    break;
                case WindowMessageType.Warning:
                    result = MessageBoxImage.Warning;
                    break;
                case WindowMessageType.Error:
                    result = MessageBoxImage.Error;
                    break;
                default:
                    throw new KeyNotFoundException("Cant find selected message type");
            }

            return result;
        }
    }
}
