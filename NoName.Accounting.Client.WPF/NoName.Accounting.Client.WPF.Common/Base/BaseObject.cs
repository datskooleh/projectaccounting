﻿using NoName.Accounting.Client.WPF.Common;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace NoName.Accounting.Client.WPF.Common.Base
{
    public abstract class BaseObject : BasePropertyChangedObject
    {
        protected Boolean SetValue<T>(ref T variable, T value, [CallerMemberName] String propertyName = null)
        {
            var setValueResult = false;
            if (!EqualityComparer<T>.Default.Equals(variable, value))
            {
                variable = value;
                OnPropertyChanged(propertyName);
                setValueResult = true;
            }

            return setValueResult;
        }
    }
}
