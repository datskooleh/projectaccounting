﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace NoName.Accounting.Client.WPF.Common.Base
{
    public abstract class BaseModelObject : BasePropertyChangedObject
    {
        private Int32 _id;
        public Int32 Id
        {
            get { return _id; }
            set { SetValue(ref _id, value); }
        }

        private Int32 _serverId;
        public Int32 ServerId
        {
            get { return _serverId; }
            set { SetValue(ref _serverId, value); }
        }

        private DateTime _lastChangedDate;
        public DateTime LastChangedDate
        {
            get { return _lastChangedDate; }
            set { SetValue(ref _lastChangedDate, value); }
        }

        protected Boolean SetValue<T>(ref T variable, T value, [CallerMemberName] String propertyName = null)
        {
            var setValueResult = false;
            if (!EqualityComparer<T>.Default.Equals(variable, value))
            {
                variable = value;
                OnPropertyChanged(propertyName);
                setValueResult = true;

                LastChangedDate = DateTime.UtcNow;
            }

            return setValueResult;
        }
    }
}
