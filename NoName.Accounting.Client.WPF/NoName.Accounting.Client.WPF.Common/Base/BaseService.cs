﻿using System;

namespace NoName.Accounting.Client.WPF.Common.Base
{
    public abstract class BaseService : BaseModelObject
    {
        private Double _price;
        private DateTime _date;

        public Double Price
        {
            get { return _price; }
            set { SetValue(ref _price, value); }
        }

        public DateTime Date
        {
            get { return _date; }
            set { SetValue(ref _date, value); }
        }
    }
}
