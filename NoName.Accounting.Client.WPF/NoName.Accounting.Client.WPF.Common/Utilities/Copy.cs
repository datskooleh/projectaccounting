﻿using NoName.Accounting.Client.WPF.Common.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace NoName.Accounting.Client.WPF.Common.Utilities
{
    public static class Copy
    {
        public static void CopyObject<T>(T source, T destination)
        {
            if (source == null)
                throw new ArgumentNullException("Source argument can't be null");
            else if (destination == null)
                throw new ArgumentNullException("Destination argument can't be null");

            var sourceProperties = source.GetType().GetProperties().Select(x => x).ToDictionary(x => x.Name);
            var targetProperties = source.GetType().GetProperties().Select(x => x).ToDictionary(x => x.Name);

            PropertyInfo property;
            foreach (var sourceProperty in sourceProperties)
            {
                if (targetProperties.TryGetValue(sourceProperty.Key, out property))
                {
                    //if (sourceProperty.Value.PropertyType == typeof(Int32))
                    //    CopyValueType(source, sourceProperty.Value, destination, property);
                    //else
                    //{
                    //    if (sourceProperty.Value.GetType().BaseType == typeof(BaseObject))
                    //        CopyObject(sourceProperty.Value, property);
                    //    else CopyReferenceType(source, sourceProperty.Value, destination, property);
                    //}

                    if (sourceProperty.Value.GetValue(source) != null)
                        CopyReferenceType(source, sourceProperty.Value, destination, property);

                    //        property.SetValue(destination, sourceProperty.Value.GetValue(source));
                }
            }
        }

        private static void CopyReferenceType<T>(T sourceOriginal, PropertyInfo source, T destinationOriginal, PropertyInfo destination)
        {
            var serialized = JsonConvert.SerializeObject(source.GetValue(sourceOriginal));
            dynamic deserialized = JsonConvert.DeserializeObject(serialized);
            var deserializedJObject = deserialized as JObject;

            if (deserializedJObject == null)
            {
                if (deserialized.GetType() == typeof(Int64))
                {
                    if (source.PropertyType.IsEnum)
                        destination.SetValue(destinationOriginal, Enum.Parse(source.PropertyType, deserialized.ToString()));
                    else
                        destination.SetValue(destinationOriginal, Convert.ToInt32(deserialized));
                }
                else
                    destination.SetValue(destinationOriginal, deserialized);
            }
            else
                destination.SetValue(destinationOriginal, deserializedJObject.ToObject(source.PropertyType));
        }

        private static void CopyValueType<T>(T sourceOriginal, PropertyInfo source, T destinationOriginal, PropertyInfo destination)
        {
            destination.SetValue(destinationOriginal, source.GetValue(sourceOriginal));
        }
    }
}
