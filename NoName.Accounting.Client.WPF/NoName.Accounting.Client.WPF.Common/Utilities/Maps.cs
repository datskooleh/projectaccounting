﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NoName.Accounting.Client.WPF.Common.Utilities
{
    public static class Maps
    {
        private static String _baseApiUrl = "http://dev.virtualearth.net/REST/V1/";
        private static String _getPathRouteUrl = _baseApiUrl + "Routes/Driving?wp.0={0}&wp.1={1}&key={2}";
        private static String _getLocationUrl = _baseApiUrl + "Locations/{0}?key={1}";

        public static void GetRoute(String startWayPoint, String endWayPoint, String key, Action<Route> callback)
        {
            if (String.IsNullOrWhiteSpace(startWayPoint) || String.IsNullOrWhiteSpace(endWayPoint) || String.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException("Aguments must not be null");

            Route routeData = null;

            TryWrapper(String.Format(_getPathRouteUrl, startWayPoint, endWayPoint, key), (jsonResponse) =>
            {
                routeData = GetFirstInstance<Route>("routeLegs", jsonResponse);

                if (callback != null)
                    callback(routeData);
            });

        }

        public static void GetLocationByAddress(String address, String key, Action<Tuple<Double, Double>> callback)
        {
            if (String.IsNullOrWhiteSpace(address) || String.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException("Aguments must not be null");

            Tuple<Double, Double> coordinates = null;

            TryWrapper(String.Format(_getLocationUrl, Uri.EscapeDataString(address), key), (jsonResponse) =>
            {
                var parsedCoordinatedList = GetFirstInstance<List<Point>>("geocodePoints", jsonResponse).First();

                if (callback != null)
                    callback(coordinates = Tuple.Create<Double, Double>(parsedCoordinatedList.coordinates[0], parsedCoordinatedList.coordinates[1]));
            });
        }

        private static T GetFirstInstance<T>(string propertyName, string json)
        {
            using (var stringReader = new StringReader(json))
            using (var jsonReader = new JsonTextReader(stringReader))
            {
                while (jsonReader.Read())
                {
                    if (jsonReader.TokenType == JsonToken.PropertyName
                        && (string)jsonReader.Value == propertyName)
                    {
                        jsonReader.Read();

                        var serializer = new JsonSerializer();
                        return serializer.Deserialize<T>(jsonReader);
                    }
                }
                return default(T);
            }
        }

        private static async void TryWrapper(String requestUrl, Action<String> actionToDo)
        {
            var tries = 0;
            while (tries < 5)
            {
                try
                {
                    using (var client = new HttpClient())
                    using (var response = await client.GetAsync(requestUrl))
                    {
                        var jsonResponse = await response.Content.ReadAsStringAsync();

                        if (actionToDo != null)
                            lock (actionToDo)
                                if (actionToDo != null)
                                    actionToDo(jsonResponse);
                        break;
                    }
                }
                catch (Exception ex)
                {
                    ++tries;
                    continue;
                }
            }
        }
    }

    //[DataContract]
    public class Point
    {
        //[DataMember(Name = "coordinates")]
        public List<Double> coordinates { get; set; }
    }

    //[DataContract]
    public class ItineraryItem
    {
        //[DataMember(Name = "maneuverPoint")]
        public Point maneuverPoint { get; set; }
    }

    //[DataContract]
    public class Route
    {
        //[DataMember(Name = "itineraryItems")]
        public List<ItineraryItem> itineraryItems { get; set; }
        //[DataMember(Name = "travelDistance")]
        public Double travelDistance { get; set; }
    }
}