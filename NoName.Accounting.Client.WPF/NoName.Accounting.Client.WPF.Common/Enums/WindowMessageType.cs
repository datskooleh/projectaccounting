﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoName.Accounting.Client.WPF.Common.Enums
{
    public enum WindowMessageType
    {
        Information,
        Question,
        Warning,
        Error
    }
}
