﻿using NoName.Accounting.Client.WPF.Common.Base;
using System;

namespace NoName.Accounting.Client.WPF.Models
{
    [Serializable]
    public sealed class Address : BaseModelObject
    {
        private String _city;
        private String _house;
        private String _street;
        private Int32? _appartment;

        public String City
        {
            get { return _city; }
            set { SetValue(ref _city, value); }
        }

        public String Street
        {
            get { return _street; }
            set { SetValue(ref _street, value); }
        }

        public String House
        {
            get { return _house; }
            set { SetValue(ref _house, value); }
        }


        public Int32? Appartment
        {
            get { return _appartment; }
            set { SetValue(ref _appartment, value); }
        }
    }
}
