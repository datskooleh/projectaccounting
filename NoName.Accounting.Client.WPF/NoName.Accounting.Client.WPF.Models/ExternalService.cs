﻿using NoName.Accounting.Client.WPF.Common.Base;
using System.Collections.Generic;

namespace NoName.Accounting.Client.WPF.Models
{
    public sealed class ExternalService : BaseService
    {
        private Client _client;
        private PathCost _travelCost;
        private readonly List<Consumable> _consumables;

        public ExternalService()
        {
            _consumables = new List<Consumable>();
        }

        public Client Client
        {
            get { return _client; }
            set { SetValue(ref _client, value); }
        }

        public PathCost TravelCost
        {
            get { return _travelCost; }
            set { SetValue(ref _travelCost, value); }
        }

        public List<Consumable> Consumables
        {
            get { return _consumables; }
        }
    }
}
