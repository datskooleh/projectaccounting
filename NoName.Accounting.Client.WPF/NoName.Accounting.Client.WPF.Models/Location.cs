﻿using NoName.Accounting.Client.WPF.Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoName.Accounting.Client.WPF.Models
{
    public sealed class Location : BaseModelObject
    {
        private String _addressLine;
        private Double _latitude;
        private Double _longitude;

        public String AddressLine
        {
            get { return _addressLine; }
            set { SetValue(ref _addressLine, value); }
        }

        public Double Latitude
        {
            get { return _latitude; }
            set { SetValue(ref _latitude, value); }
        }

        public Double Longitude
        {
            get { return _longitude; }
            set { SetValue(ref _longitude, value); }
        }
    }
}
