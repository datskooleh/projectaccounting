﻿using NoName.Accounting.Client.WPF.Common.Base;
using System;

namespace NoName.Accounting.Client.WPF.Models
{
    public sealed class OrderItem : BaseModelObject
    {
        private Article _article;
        private Double _price;
        private Double _count;
        private Double _totalPrice;

        public Article Article
        {
            get { return _article; }
            set { SetValue(ref _article, value); }
        }

        public Double Price
        {
            get { return _price; }
            set { SetValue(ref _price, value); }
        }

        public Double Count
        {
            get { return _count; }
            set { SetValue(ref _count, value); }
        }

        public Double TotalPrice
        {
            get { return _totalPrice; }
            set { SetValue(ref _totalPrice, value); }
        }
    }
}
