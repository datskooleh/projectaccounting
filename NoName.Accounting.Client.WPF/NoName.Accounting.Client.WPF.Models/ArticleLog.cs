﻿using NoName.Accounting.Client.WPF.Common.Base;
using System;

namespace NoName.Accounting.Client.WPF.Models
{
    public sealed class ArticleLog : BaseModelObject
    {
        private Article _article;
        public Article Article
        {
            get { return _article; }
            set { SetValue(ref _article, value); }
        }

        private Double _newPrice;
        public Double NewPrice
        {
            get { return _newPrice; }
            set { SetValue(ref _newPrice, value); }
        }

        private DateTime _date;
        public DateTime Date
        {
            get { return _date; }
            set { SetValue(ref _date, value); }
        }
    }
}
