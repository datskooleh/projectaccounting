﻿using NoName.Accounting.Client.WPF.Common.Base;
using NoName.Accounting.Client.WPF.Models.Enums;
using System;

namespace NoName.Accounting.Client.WPF.Models
{
    public sealed class Repair : BaseService
    {
        private String _name;
        public String Name
        {
            get { return _name; }
            set { SetValue(ref _name, value); }
        }

        private RepairStatus _status;
        public RepairStatus Status
        {
            get { return _status; }
            set { SetValue(ref _status, value); }
        }

        private DateTime? _dateReturned;
        public DateTime? DateReturned
        {
            get { return _dateReturned; }
            set { SetValue(ref _dateReturned, value); }
        }

        private String _description;
        public String Description
        {
            get { return _description; }
            set { SetValue(ref _description, value); }
        }
    }
}
