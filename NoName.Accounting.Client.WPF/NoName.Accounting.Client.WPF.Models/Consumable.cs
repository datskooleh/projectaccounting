﻿using NoName.Accounting.Client.WPF.Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoName.Accounting.Client.WPF.Models
{
    public sealed class Consumable : BaseModelObject
    {
        public String Name { get; set; }

        public Double BuyPrice { get; set; }

        public Double SellPrice { get; set; }
    }
}
