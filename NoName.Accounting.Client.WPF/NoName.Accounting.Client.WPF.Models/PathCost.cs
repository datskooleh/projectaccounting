﻿using NoName.Accounting.Client.WPF.Common.Base;
using System;

namespace NoName.Accounting.Client.WPF.Models
{
    public sealed class PathCost : BaseModelObject
    {
        private Double _fuelPricePerLitr;
        private Double _fuelConsumptionPerLitr;
        private Double _routeLength;
        private Location _startLocation;
        private Location _endLocation;
        private Boolean _isTwoWay;

        public PathCost()
        {
            StartLocation = new Location();
            EndLocation = new Location();
        }

        public Double FuelPricePerLitr
        {
            get { return _fuelPricePerLitr; }
            set { SetValue(ref _fuelPricePerLitr, value); }
        }

        public Double FuelConsumption
        {
            get { return _fuelConsumptionPerLitr; }
            set { SetValue(ref _fuelConsumptionPerLitr, value); }
        }

        public Double RouteLength
        {
            get { return _routeLength; }
            set { SetValue(ref _routeLength, value); }
        }

        public Location StartLocation
        {
            get { return _startLocation; }
            set { SetValue(ref _startLocation, value); }
        }

        public Location EndLocation
        {
            get { return _endLocation; }
            set { SetValue(ref _endLocation, value); }
        }

        public Boolean IsTwoWay
        {
            get { return _isTwoWay; }
            set { SetValue(ref _isTwoWay, value); }
        }

        public Double TotalCost { get { return RouteLength / 100.0 * FuelConsumption * FuelPricePerLitr * (IsTwoWay ? 2 : 1); } }
    }
}
