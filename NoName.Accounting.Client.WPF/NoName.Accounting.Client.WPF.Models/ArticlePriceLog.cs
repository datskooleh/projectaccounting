﻿using NoName.Accounting.Client.WPF.Common.Base;
using System;

namespace NoName.Accounting.Client.WPF.Models
{
    public sealed class ArticlePriceLog : BaseModelObject
    {
        private DateTime _date;
        private Double _price;
        private Article _good;

        public DateTime Date
        {
            get { return _date; }
            set { SetValue(ref _date, value); }
        }

        public Double Price
        {
            get { return _price; }
            set { SetValue(ref _price, value); }
        }

        public Article Good
        {
            get { return _good; }
            set { SetValue(ref _good, value); }
        }
    }
}
