﻿using System;

namespace NoName.Accounting.Client.WPF.Models.Enums
{
    [Flags]
    public enum RepairStatus : byte
    {
        Accepted,
        InProgress,
        Done,
        CantBeDone
    }
}
