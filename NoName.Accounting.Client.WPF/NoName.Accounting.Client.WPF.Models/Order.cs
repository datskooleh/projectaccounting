﻿using NoName.Accounting.Client.WPF.Common.Base;
using System.Collections.Generic;
using System.Linq;

namespace NoName.Accounting.Client.WPF.Models
{
    public sealed class Order : BaseService
    {
        private ICollection<OrderItem> _orderItem;

        public ICollection<OrderItem> OrderItems
        {
            get { return _orderItem.ToList(); }
            set
            {
                Price = 0.0;

                lock (value)
                    if (value != null)
                        Price = _orderItem.Sum(x => x.TotalPrice);

                SetValue(ref _orderItem, value);

            }
        }
    }
}
