﻿using NoName.Accounting.Client.WPF.Common.Base;
using System;
using System.Collections.Generic;

namespace NoName.Accounting.Client.WPF.Models
{
    public sealed class Client : BaseModelObject
    {
        private String _firstName;
        private String _lastName;
        private Address _address;
        private String _phone;

        public Client()
        {
            Address = new Address();
        }

        public String FirstName
        {
            get { return _firstName; }
            set { SetValue(ref _firstName, value); }
        }

        public String LastName
        {
            get { return _lastName; }
            set { SetValue(ref _lastName, value); }
        }

        public String Phone
        {
            get { return _phone; }
            set { SetValue(ref _phone, value); }
        }

        public Address Address
        {
            get { return _address; }
            set { SetValue(ref _address, value); }
        }
    }
}
