﻿using NoName.Accounting.Client.WPF.Common.Base;
using System;

namespace NoName.Accounting.Client.WPF.Models
{
    public sealed class Article : BaseModelObject
    {
        private String _title;
        private String _description;
        private Double _price;
        private Double _count;

        public String Title
        {
            get { return _title; }
            set { SetValue(ref _title, value); }
        }

        public String Description
        {
            get { return _description; }
            set { SetValue(ref _description, value); }
        }

        public Double Price
        {
            get { return _price; }
            set { SetValue(ref _price, value); }
        }

        public Double Count
        {
            get { return _count; }
            set { SetValue(ref _count, value); }
        }
    }
}
